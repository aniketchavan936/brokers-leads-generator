import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
const appRoutes: Routes = [
  {path:'', loadChildren:() => import('./landing/landing-routing.module').then(m =>m.LandingRoutingModule)},
  {path:'login', loadChildren:() => import('./login/login.module').then(m =>m.LoginModule)},
  {path: 'create-data', canActivate: [AuthGuard], loadChildren:()=> import('./screens/create/create.module').then(m =>m.CreateModule)},
  {path: 'manage-leads', canActivate: [AuthGuard], loadChildren:()=> import('./screens/my-leads/my-leads.module').then(m =>m.MyLeadsModule)},
  {path: '**', redirectTo:'home'},
];
// 
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
