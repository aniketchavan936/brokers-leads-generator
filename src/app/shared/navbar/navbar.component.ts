import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  links : any[]= ["brokersFisrtChoice.com","puneBuildersHK.com"];

  mailText:string = "";
  constructor(public router:Router, private service: LoginService) { }
  loggedIn = false;
  ngOnInit(): void {
    this.mailText = "mailto:aniketchavan936@gmail.com?subject=flat enquiry...&body="+this.links.join(" ,");
    if(localStorage.getItem('user')){
      this.loggedIn = true;
    } else {
      this.loggedIn = false;
    }
  }
  gotoHome() {
    this.router.navigate(['home']);
  }
  gotoRegister() {
    this.router.navigate(['register']);
  }
  gotoAboutUs() {
    this.router.navigate(['about']);
  }
  gotoLogin() {
    this.router.navigate(['login']);
  }
  gotoCreate(){
    this.router.navigate(['create-data']);
  }
  Logout() {
    this.service.Logout();
  }
  gotoLeads() {
    this.router.navigate(['manage-leads']);
  }
}
