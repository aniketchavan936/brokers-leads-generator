import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpInterceptor } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  public jwtToken:any;
  public urlsToNotUse:any;
  constructor(private router: Router) { }
  intercept(request , execute) {
    console.log('')
    this.jwtToken  = localStorage.getItem('jwtToken');
    // const tokenrequest = request.clone({
    //   setHeaders: {
    //     Authorization: `Bearer ${this.jwtToken}`
    //   }
    // });
    return execute.handle(request)
    .pipe(catchError((error: HttpErrorResponse) => {
      if (error.status === 401) {
        setTimeout(() => {
          this.router.navigate(['home']);
        }, 2000);
      }
      return throwError(error);
    }));
  }
}
