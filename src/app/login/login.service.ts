import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoginRoutingModule } from './login-routing.module';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private angularFire: AngularFireAuth,public router:Router) { }

  async loginAuth(data) {
    await this.angularFire.signInWithEmailAndPassword(data.email,data.password)
    .then(async (res) =>{
      await localStorage.setItem('user', JSON.stringify(res));
      if(localStorage.getItem('user')) {
        this.router.navigate(['home']);
      }
    })
  }

  async signUpAuth(data) {
    await this.angularFire.createUserWithEmailAndPassword(data.email,data.password)
    .then(async (res) => {
      await localStorage.setItem('user', JSON.stringify(res));
      if(localStorage.getItem('user')) {
        this.router.navigate(['home'])
      }
    });
  }
  async Logout() {
    console.log('logout called');
    localStorage.clear();
    await this.angularFire.signOut();
    this.router.navigate(['login']);
  }
}


// return new Promise((resolve, reject) => {
//   this.angularFire.signInWithEmailAndPassword(data.email,data.password).then(
//     res => resolve(res),
//     err => reject(err)
//   );
// });
