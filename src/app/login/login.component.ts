import { Component, OnInit, Renderer2, Inject, OnDestroy } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public login: FormGroup;
  public signUp: FormGroup;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private service: LoginService,
    public router:Router
  ) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'login-background-image');
    this.login = new FormGroup({
      email: new FormControl('',[Validators.required,Validators.email]),
      password: new FormControl('',[Validators.required,Validators.minLength(7)]),
    });
    this.signUp = new FormGroup({
      email: new FormControl('',[Validators.required,Validators.email]),
      password: new FormControl('',[Validators.required,Validators.minLength(7)]),
      confirmPassword: new FormControl('',[Validators.required,Validators.minLength(7)])
    });
  }
  ngOnDestroy() {
    this.renderer.removeClass(this.document.body, 'login-background-image');
  }
  Login() {
    if(this.login.status == 'VALID') {
      this.service.loginAuth(this.login.value);
    }
  }
  Sign() {
    if(this.signUp.status== 'VALID') {
      if(this.signUp.value.password == this.signUp.value.confirmPassword) {
        this.service.signUpAuth(this.signUp.value);
      }
    }
  }
  gotoHome() {
    this.router.navigate(['home']);
  }
}
