import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
interface DataRef {
  id?:String;
  trackableImageUrl: String, 
  projectName: String,
  flatType: String,
  projectStartFrom: Number,
  projectEndsAt: Number,
  projectId?: Number,
  ProjectLocation: String,
  description?: String,
}
interface Lead {
  id?:String;
  name: String;
  email: String;
  mobile: Number;
  project: String;
  time?: String;
  date?: String;
  query?: String;
}
@Injectable({
  providedIn: 'root'
})
export class FireStoreService {
  propertyData: AngularFirestoreCollection<DataRef>;
  leadData: AngularFirestoreCollection<Lead>;
  items:Observable<DataRef[]>;
  leads:Observable<Lead[]>;
  itemDoc:AngularFirestoreDocument<DataRef>;
  leadDoc:AngularFirestoreDocument<Lead>;
  constructor(public fireStre: AngularFirestore) {
    // this.items = this.fireStre.collection('items').valueChanges();
    this.leadData = this.fireStre.collection('leads', ref=> ref.orderBy('name'));
    this.leads = this.leadData.snapshotChanges().pipe(map(changes =>{
      return changes.map(a => {
        const data = a.payload.doc.data() as Lead;
        data.id = a.payload.doc.id;
        return data;
      })
    }));
    this.propertyData = this.fireStre.collection('properties', ref => ref.orderBy('flatType.bhk', 'asc'));
    this.items = this.propertyData.snapshotChanges().pipe(map(changes =>{
      return changes.map(a => {
        const data = a.payload.doc.data() as DataRef;
        data.id = a.payload.doc.id;
        return data;
      })
    }));
   }
  //  get
   getItem() {
     return this.items;
   }
   getLeads() {
     return this.leads;
   }
  //  set
  addItem(data) {
  this.propertyData.add(data);
  }
  addLeadItem(data) {
  this.leadData.add(data);
  }
  // delete
  deleteItem(data){
    this.itemDoc = this.fireStre.doc(`properties/${data.id}`);
    this.itemDoc.delete();
  }
  deleteLead(data) {
    this.leadDoc = this.fireStre.doc(`leads/${data.id}`);
    this.leadDoc.delete();
  }
}
