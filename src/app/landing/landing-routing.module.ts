import { NgModule, ViewChild } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from '../screens/about-us/about-us.component';
import { HomeComponent } from '../screens/home/home.component';
import { RegisterComponent } from '../screens/register/register.component';
import { LandingComponent } from './landing.component';

const routes: Routes = [
  {
    path: '', component:LandingComponent,
    children:[
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'prefix'
     },
     {path:'home',component:HomeComponent},
     {path:'register',component:RegisterComponent},
     {path:'about',component:AboutUsComponent},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
