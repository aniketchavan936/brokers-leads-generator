import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FireStoreService } from 'src/app/fire-store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public router:Router,public fireService: FireStoreService) { }
  propertiesData: any;
  ngOnInit(): void {
    this.fireService.getItem().subscribe(item => { this.propertiesData = item });
  }
  gotoRegister() {
    this.router.navigate(['register']);
  }
}
