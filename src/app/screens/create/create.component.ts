import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirePerformance } from '@angular/fire/performance';
import { Observable } from 'rxjs';
import { FireStoreService } from '../../fire-store.service';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public register: FormGroup;
  public today =  new Date();
  public progressValue: Observable<number>;
  public filepath:any;
  public filename:any;
  public downloadableURL = '';
  public items: Observable<any>;
  public flatType = [{bhk: '1BHK'},{bhk: '2BHK'},{bhk: '3BHK'},{bhk: '4BHK'},{bhk: '1 & 2BHK'},{bhk: '1 & 3BHK'},{bhk: '2 & 3BHK'},{bhk: '2 & 4BHK'},{bhk: '1 & 4BHK'}];
  public task: AngularFireUploadTask;

  constructor(public router:Router,
    private performance: AngularFirePerformance,
    private angularFire: AngularFireStorage,
    public fireService: FireStoreService) { }

  ngOnInit(): void {

    this.register = new FormGroup({
      trackableImageUrl: new FormControl('',[Validators.required]), 
      projectName: new FormControl('',[Validators.required]),
      flatType: new FormControl('',[Validators.required,Validators.minLength(10)]),
      projectStartFrom: new FormControl('',[Validators.required]),
      projectEndsAt: new FormControl('',[Validators.required]),
      projectId: new FormControl(''),
      ProjectLocation: new FormControl('',[Validators.required]),
      description:new FormControl(''),
    });
  }

  async imageUpload(file) {
    const fileData = file.target.files[0];
    if (fileData) {
      this.filename = file.target.files[0].name;
      this.filepath =  Math.random() + fileData;
      this.performance.trace('image upload performance').then(success => {
        console.log('success', success);
      });
      this.task = this.angularFire.upload(`PropertyImages/${this.filepath}`, fileData);
      this.progressValue = this.task.percentageChanges();
      (await this.task).ref.getDownloadURL().then(url => {
        this.downloadableURL = url;
        this.register.get('trackableImageUrl').setValue(url);
      });
    } else {
      alert('No image selected');
      this.downloadableURL = '';
    }
  }

  async submitForm() {
    console.log('submited',this.register);
    if(this.register.status =='VALID') {
      await this.fireService.addItem(this.register.value);
      this.gotoHome();
    }
  }

  async gotoHome() {
    await this.router.navigate(['home']);
  }
}
