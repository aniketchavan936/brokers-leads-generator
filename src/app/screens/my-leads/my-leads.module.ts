import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyLeadsRoutingModule } from './my-leads-routing.module';
import { MyLeadsComponent } from './my-leads.component';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [
    MyLeadsComponent
  ],
  imports: [
    CommonModule,
    MyLeadsRoutingModule,
    MatListModule,
    MatTabsModule,
    MatIconModule,
    MatExpansionModule
  ]
})
export class MyLeadsModule { }
