import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FireStoreService } from 'src/app/fire-store.service';

@Component({
  selector: 'app-my-leads',
  templateUrl: './my-leads.component.html',
  styleUrls: ['./my-leads.component.scss']
})
export class MyLeadsComponent implements OnInit {
  propertiesData: any;
  leadData:any;
  constructor(public router:Router,public fireService: FireStoreService) { }

  ngOnInit(): void {
    this.fireService.getItem().subscribe(item => { this.propertiesData = item });
    this.fireService.getLeads().subscribe(item => { this.leadData = item });
  }
  delete(event, item){
    this.fireService.deleteItem(item);
  }
  deleteLead(event, item) {
    this.fireService.deleteLead(item);
  }
  gotoHome() {
    this.router.navigate(['home']);
  }

}
