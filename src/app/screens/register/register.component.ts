import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FireStoreService } from 'src/app/fire-store.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public register: FormGroup;
  public today =  new Date();
  public projects = [];
  public mytime:any;

  constructor(public router:Router,public fireService: FireStoreService) { }

  ngOnInit(): void {
    this.fireService.getItem().subscribe(item=>{
      this.projects = item;
      console.log('project');
    });
    this.register = new FormGroup({
      name: new FormControl('',[Validators.required]),
      email: new FormControl('',[Validators.required,Validators.email]),
      mobile: new FormControl('',[Validators.required,Validators.minLength(10)]),
      project: new FormControl('',[Validators.required]),
      time: new FormControl(''),
      date: new FormControl(''),
      query:new FormControl(''),
    });
  }
  async submitForm() {
    if(this.register.status == 'VALID') {
      await this.fireService.addLeadItem(this.register.value);
      this.gotoHome();
    }
  }

  gotoHome() {
    this.router.navigate(['home']);
  }
}
