import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingComponent } from './landing/landing.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

// firbase dependancies
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FooterComponent } from './shared/footer/footer.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';

// material dependencies
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from "@angular/material/button";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatNativeDateModule } from '@angular/material/core';

// other modules
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AboutUsModule } from './screens/about-us/about-us.module';
import { RegisterModule } from './screens/register/register.module';
import { HomeModule } from './screens/home/home.module';

// auth
import { AuthGuard } from './auth.guard';
import { InterceptorService } from './interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FireStoreService } from './fire-store.service';

const firebaseConfig = {
  apiKey: "AIzaSyD1NGkQOZCn8VDJ9o0AJrQjZTS2ce9pK04",
  authDomain: "brokersinfo-d98ec.firebaseapp.com",
  projectId: "brokersinfo-d98ec",
  storageBucket: "brokersinfo-d98ec.appspot.com",
  messagingSenderId: "58871543219",
  appId: "1:58871543219:web:04b259450e00c2353787c6",
  measurementId: "G-EXMRF2BH9V"
};

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    NavbarComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,


    // firebase imports
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage
    AngularFireDatabaseModule, // for database


    // material imports
    MatDividerModule,
    TimepickerModule.forRoot(),
    PopoverModule.forRoot(),
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,

    // component module
    AboutUsModule,
    RegisterModule,
    HomeModule,
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    FireStoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
